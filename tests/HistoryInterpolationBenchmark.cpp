#include "../src/DataHistory/VADoubleHistoryModel.cpp"
#include "../src/DataHistory/VADoubleHistoryModel.h"

#include <stdio.h>
#include <iostream>
#include <math.h>
#include <cmath>

#include <chrono>
#include <vector>
#include <numeric>
#include <algorithm>
#include <functional>

typedef struct Statistics
{
	double mean;
	double median;
	double stdev;
} Statistics;

double calculateStdDev(const std::vector<double> vector, const double mean) 
{
	double tmp = 0;
	for( double val : vector )
	{
		tmp += ( val - mean ) * ( val - mean ); 
	}

	double stdDev = sqrt( tmp / vector.size( ) );

	return stdDev;
}

Statistics getStats( const std::vector<double> &i_vector )
{
	Statistics stats;
	std::vector<double> vector( i_vector );
	std::sort( vector.begin( ), vector.end( ) );
	double sumD = std::accumulate( vector.begin( ), vector.end( ), 0.0 );
	stats.mean  = sumD / double( vector.size( ) );
	
	stats.stdev = calculateStdDev( vector, stats.mean );
	
	stats.median = vector[std::floor( double( vector.size( ) ) / 2.0 )];
	stats.stdev  = std::sqrt( stats.stdev / vector.size( ) );
	return stats;
}


void main( )
{
	const int iInputSize = 1000000;
	double dInputUpdateRate = 20;

	const std::vector<int> viUpsamplingFactors = { 1, 2, 5, 10, 20, 50, 100 };

	const int iBufferSize = 1000;

	const int iOutputSize = iInputSize;

	std::vector<double> vdWindowSize;
	double dWindowDelay = 0;
	int iDegree         = 1;

	Statistics datastats;

	double dInputTimeStamp;
	double dInputSample;

	double dOutputTimeStamp;
	double dOutputSample;

	double dTimePeriod = iInputSize/dInputUpdateRate;

	const int identifier[] = { 0, 1, 2, 3, 10};

	std::chrono::duration<double, std::micro> dTimeInterval;
	
	std::ofstream file;
	file.open( "D:\\HIWI\\work\\code\\base_data\\interpolationBenchmark.csv" );

	std::cout << (double)std::chrono::steady_clock::period::num / std::chrono::steady_clock::period::den << std::endl;

	for( int iUpsamplingID = 0; iUpsamplingID < viUpsamplingFactors.size(); iUpsamplingID++ )
	{
		
		const int iUpsamplingFactor = viUpsamplingFactors[iUpsamplingID];

		std::vector<double> vTimeIntervals( iOutputSize );

		std::cout << "Upsampling factor: " << iUpsamplingFactor << std::endl;

		for( int i = 0; i < 5; i++ )
		{
			CVAHistoryEstimationMethod::EMethod eMethod = static_cast<CVAHistoryEstimationMethod::EMethod>( identifier[i] );

			if( eMethod == CVAHistoryEstimationMethod::EMethod::TriangularWindow )
			{
				vdWindowSize = { 5.0, 3.333, 2.5, 1.666, 1.0, 0.5, 0.3333, 0.25, 0.1666, 0.1 };
			}
			else
			{
				vdWindowSize = { 1.0 };
			}


			for( int l = 0; l < vdWindowSize.size( ); l++ )
			{
				CVADoubleHistoryModel oHistoryBuffer = CVADoubleHistoryModel( iBufferSize, eMethod, vdWindowSize[l], dWindowDelay, iDegree );

				std::cout << "   ->  " << oHistoryBuffer.GetEstimationMethod( ).ToString( ) << ", " << vdWindowSize[l] << "s window size" << std::endl;

				file << iUpsamplingFactor << "," << identifier[i] << "," << vdWindowSize[l] << ",";


 				for( int j = 0; j < iOutputSize; j++ )
				{

					dOutputTimeStamp = dTimePeriod * double( j ) / double( iInputSize * iUpsamplingFactor ) + 0.001;

					if( j == 0 )
					{
						for( int i = 0; i < iBufferSize; i++ )
						{
							dInputTimeStamp = dTimePeriod * ( double( i ) / double( iInputSize ));
							dInputSample    = std::sin( dInputTimeStamp * 2 * ITAConstants::PI_D );
								
							oHistoryBuffer.Push( dInputTimeStamp, std::make_unique<double>( dInputSample ) );
						}
					}
					else if( ( j % ( iBufferSize * iUpsamplingFactor / 2) ) == 0 && (j != 0) )
					{
						for( int i = 1; i <= double(iBufferSize)/2; i++ )
						{
							dInputTimeStamp = dTimePeriod * ( 1.0 / double( iInputSize ) ) + oHistoryBuffer.GetLastPushedTimestamp( );
							dInputSample    = std::sin( dInputTimeStamp * 2 * ITAConstants::PI_D );

							oHistoryBuffer.Push( dInputTimeStamp, std::make_unique<double>( dInputSample ) );
						}
					}

					auto ti = std::chrono::steady_clock::now( );

					oHistoryBuffer.Update( dOutputTimeStamp ); // important to keep this here because it differs for sliding window
					oHistoryBuffer.Estimate( dOutputTimeStamp, dOutputSample );

					auto tf = std::chrono::steady_clock::now( );

					dTimeInterval = tf - ti;

					vTimeIntervals[j] = dTimeInterval.count( );
				}

				datastats = getStats( vTimeIntervals );

				file << datastats.mean << "," << datastats.stdev << "," << datastats.median << std::endl;
				std::cout << "            " << datastats.mean << "," << datastats.stdev << "," << datastats.median << std::endl;
			}
		}
	}

 
	file.close( );

};

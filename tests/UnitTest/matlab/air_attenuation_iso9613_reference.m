distance = 1000;

f = ita_ANSI_center_frequencies;
temperature = 20;
humidity = 50;
static_pressure = 101325 / 1000;
alpha_iso = ita_atmospheric_absorption_iso9613( f, temperature, humidity, static_pressure );

attenuationDB = alpha_iso * distance;

attenuation = 10.^(-attenuationDB./20);
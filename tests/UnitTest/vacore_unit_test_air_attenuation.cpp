#include <Filtering/VAAirAttenuationISO9613.h>
#include <ITABase/ISO9613/ITAISO9613ReferenceValues.h>
#include <ITAThirdOctaveMagnitudeSpectrum.h>
#include <catch2/catch_all.hpp>
#include <cmath>

bool GainsBelowMaxdBDeviation( float fGain1, float fGain2, float fMaxDB = 1.0f )
{
	const float fDeltaDB = std::abs( 10.0f * std::log10( fGain1 / fGain2 ) );
	return fDeltaDB < fMaxDB;
}

TEST_CASE( "Air attenuation - vs Matlab", "[VACore][SoundPropagationEffects][AirAttenuation][PrioHigh]" )
{
	const double dDistance = 1000;
	CVAHomogeneousMedium oMedium;
	oMedium.dRelativeHumidityPercent     = 50;
	oMedium.dStaticPressurePascal        = 101325;
	oMedium.dTemperatureDegreeCentigrade = 20;

	ITABase::CThirdOctaveGainMagnitudeSpectrum oTargetSpectrum;
	oTargetSpectrum.SetMagnitudes( {
	    0.998536890878868,    0.997719227169456,    0.996392783256457,    0.994219657310443,   0.991050845553085,    0.986213889637881,    0.977962623731101,
	    0.966733419575501,    0.950627779349057,    0.929353097991350,    0.895995553680548,   0.860027843300004,    0.818126661177357,    0.772774903068461,
	    0.730454709126799,    0.686449186175247,    0.637748261345575,    0.584471590656585,   0.518117858237096,    0.424100877718855,    0.320368066795154,
	    0.207960132832816,    0.103387627843172,    0.0328642388799463,   0.00613858826563663, 0.000370635052516455, 5.43818169650169e-06, 1.14306817854353e-08,
	    1.31514258916685e-12, 5.92855652908632e-19, 6.19729463743610e-27,
	} );

	ITABase::CThirdOctaveGainMagnitudeSpectrum oOutputSpectrum;
	VA::AirAttenuationISO9613( oOutputSpectrum, dDistance, oMedium );

	const int idx6350Hz = 25;
	for( int idx = 0; idx < idx6350Hz; idx++ ) // Below 6 kHz
	{
		REQUIRE( GainsBelowMaxdBDeviation( oOutputSpectrum[idx], oTargetSpectrum[idx], 0.1f ) );
	}
	for( int idx = idx6350Hz; idx < oTargetSpectrum.GetNumBands( ); idx++ ) // Above 6 kHz
	{
		REQUIRE( GainsBelowMaxdBDeviation( oOutputSpectrum[idx], oTargetSpectrum[idx], 1.0f ) );
	}
}

TEST_CASE( "Air attenuation", "[VACore][SoundPropagationEffects][AirAttenuation][PrioHigh]" )
{

	const double dDistance = 100;
	CVAHomogeneousMedium oMedium;
	ITABase::CThirdOctaveGainMagnitudeSpectrum oOutputSpectrum;
	ITABase::CThirdOctaveGainMagnitudeSpectrum oTargetSpectrum;
	ITABase::CThirdOctaveDecibelMagnitudeSpectrum oTargetSpectrumDB;

	auto idxISODataCase = GENERATE( range( ITABase::ISO9613::CReferenceDataSet::MinimumCaseID( ), ITABase::ISO9613::CReferenceDataSet::MaximumCaseID( ) + 1 ) );
	auto oReference     = ITABase::ISO9613::CReferenceDataSet::Create( idxISODataCase );
	oTargetSpectrum     = oReference.GainSpectrum( (float)dDistance );
	oTargetSpectrumDB   = oReference.SpectrumDB( (float)dDistance );

	oMedium.dRelativeHumidityPercent     = oReference.RelativeHumidity( );
	oMedium.dStaticPressurePascal        = oReference.StaticPressurePa( );
	oMedium.dTemperatureDegreeCentigrade = oReference.TemperatureDegC( );
	VA::AirAttenuationISO9613( oOutputSpectrum, dDistance, oMedium );

	const int idx50Hz   = 4;
	const int idx2000Hz = 20;
	const int idx3150Hz = 22;
	const int idx6350Hz = 25;
	const int idx10kHz  = 27;
	float fToleranceDB;
	for( int idx = idx50Hz; idx < idx10kHz; idx++ )
	{
		if( idx < idx2000Hz )
			fToleranceDB = 0.1f;
		else if( idx < idx3150Hz )
			fToleranceDB = 0.2f;
		else if( idx < idx6350Hz )
			fToleranceDB = 0.35f;
		else
			fToleranceDB = 1.0f;

		REQUIRE( GainsBelowMaxdBDeviation( oOutputSpectrum[idx], oTargetSpectrum[idx], fToleranceDB ) );
	}
}
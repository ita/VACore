#include <Filtering/VATemporalVariations.h>
#include <catch2/catch_all.hpp>

TEST_CASE( "Proof of concept", "[VACore][ProofOfConcept][PrioHigh]" )
{
	CVAAtmosphericTemporalVariations obj;

	REQUIRE( obj.GetSamplerate( ) == 10 );
}
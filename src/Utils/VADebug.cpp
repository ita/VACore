/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include "VADebug.h"

#include "../VALog.h"

#include <ITACriticalSection.h>
#include <stdarg.h>
#include <stdio.h>

#ifdef WIN32

#	include <windows.h>

#endif

#define DEBUG_PRINTF_BUFSIZE 16384

// Lock f�r den Puffer in den Ausgabe-Routinen
static ITACriticalSection g_csDebugPrintf;
static char g_pszDebugPrintfBuf[16384];

void VA_DEBUG_PRINTF( const char* format, ... )
{
	g_csDebugPrintf.enter( );
	va_list args;
	va_start( args, format );
	vsprintf_s( g_pszDebugPrintfBuf, DEBUG_PRINTF_BUFSIZE, format, args );
	va_end( args );

	VACORE_DEBUG( g_pszDebugPrintfBuf );
	g_csDebugPrintf.leave( );
}

void VA_DEBUG_PRINTF( int, int, const char* format, ... )
{
	g_csDebugPrintf.enter( );
	va_list args;
	va_start( args, format );
	vsprintf_s( g_pszDebugPrintfBuf, DEBUG_PRINTF_BUFSIZE, format, args );
	va_end( args );

	VACORE_DEBUG( g_pszDebugPrintfBuf );
	g_csDebugPrintf.leave( );
}

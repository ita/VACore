/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */
#include "VALog.h"

#include <VistaInterProcComm/Concurrency/VistaPriority.h>
#include <algorithm>

int VALog_GetLogLevel( )
{
	auto eCurrentLevel = IHTA::Instrumentation::LoggerRegistry::get_logger( std::string( VA::VACore::logger_name ) )->level( );
	switch( eCurrentLevel )
	{
		case spdlog::level::level_enum::critical:
			return IVAInterface::ErrorLevel::VA_LOG_LEVEL_ERROR;
		case spdlog::level::level_enum::err:
			return IVAInterface::ErrorLevel::VA_LOG_LEVEL_ERROR;
		case spdlog::level::level_enum::warn:
			return IVAInterface::ErrorLevel::VA_LOG_LEVEL_WARNING;
		case spdlog::level::level_enum::info:
			return IVAInterface::ErrorLevel::VA_LOG_LEVEL_INFO;
		case spdlog::level::level_enum::debug:
			return IVAInterface::ErrorLevel::VA_LOG_LEVEL_VERBOSE;
		case spdlog::level::level_enum::trace:
			return IVAInterface::ErrorLevel::VA_LOG_LEVEL_TRACE;
	}
}

void VALog_SetLogLevel( int iLevel )
{
	if( iLevel < 0 )
		VA_EXCEPT2( INVALID_PARAMETER, "Log level cannot be negative, zero will put VACore to quite mode." );

	auto eNewLevel = spdlog::level::level_enum::off;

	if( iLevel == IVAInterface::ErrorLevel::VA_LOG_LEVEL_ERROR )
	{
		eNewLevel = spdlog::level::level_enum::err;
	}
	else if( iLevel == IVAInterface::ErrorLevel::VA_LOG_LEVEL_WARNING )
	{
		eNewLevel = spdlog::level::level_enum::warn;
	}
	else if( iLevel == IVAInterface::ErrorLevel::VA_LOG_LEVEL_INFO )
	{
		eNewLevel = spdlog::level::level_enum::info;
	}
	else if( iLevel == IVAInterface::ErrorLevel::VA_LOG_LEVEL_VERBOSE )
	{
		eNewLevel = spdlog::level::level_enum::debug;
	}
	else if( iLevel == IVAInterface::ErrorLevel::VA_LOG_LEVEL_TRACE )
	{
		eNewLevel = spdlog::level::level_enum::trace;
	}

	IHTA::Instrumentation::LoggerRegistry::get_logger( std::string( VA::VACore::logger_name ) )->set_level( eNewLevel );
}

/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include "core.h"

bool CVACoreImpl::GetUpdateLocked( ) const
{
	VA_CHECK_INITIALIZED;
	return ( m_bSyncMod );
}

void CVACoreImpl::LockUpdate( )
{
	m_dSyncEntryTime = m_pClock->getTime( );

	VA_CHECK_INITIALIZED;
	VA_LOCK_REENTRANCE;

	if( m_bSyncMod )
	{
		VA_UNLOCK_REENTRANCE;
		VA_WARN( "Lock scene", "Attempt to enter synchronized scene modification ignored - already in a synchronized scene modification" );
		//TODO: Add a warning to client once possible
		return;
	}

	VA_TRACE( "Core", "Beginning synchronized scene modification" );
	m_bSyncMod = true;

	VA_TRY
	{
		m_pNewSceneState = m_pSceneManager->CreateDerivedSceneState( m_pSceneManager->GetHeadSceneStateID( ), m_dSyncEntryTime );
		VA_UNLOCK_REENTRANCE;
	}
	VA_FINALLY
	{
		m_bSyncMod = false;
		VA_UNLOCK_REENTRANCE;
		throw;
	}
}

int CVACoreImpl::UnlockUpdate( )
{
	VA_CHECK_INITIALIZED;
	VA_LOCK_REENTRANCE;

	int iNewSceneID = -1;
	VA_TRACE( "Core", "Unlocking scene" );

	if( m_bSyncMod == false )
	{
		VA_UNLOCK_REENTRANCE;
		VA_WARN( "Unlock scene", "Not within a synchronized modification phase: scene updates are already unlocked" );
		//TODO: Make the following exception a warning to client once possible:
		//VA_EXCEPT2( MODAL_ERROR, "Not within a synchronized modification phase" );
		return -1;
	}

	VA_TRY
	{
		m_pNewSceneState->Fix( );
		iNewSceneID = m_pNewSceneState->GetID( );
		m_pSceneManager->SetHeadSceneState( iNewSceneID );

		m_bSyncMod = false;
		VA_UNLOCK_REENTRANCE;
	}
	VA_FINALLY
	{
		VA_UNLOCK_REENTRANCE;
		throw;
	}
	VA_TRY
	{
		m_pCoreThread->Trigger( );
		m_pEventManager->BroadcastEvents( );
		return iNewSceneID;
	}
	VA_RETHROW;
}

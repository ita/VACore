target_sources (
	${LIB_TARGET}
	PRIVATE VAContainerState.cpp
			VAContainerState.h
			VASoundReceiverDesc.h
			VASoundReceiverState.cpp
			VASoundReceiverState.h
			VAMotionState.cpp
			VAMotionState.h
			VASoundPortalDesc.h
			VASoundPortalState.cpp
			VASoundPortalState.h
			VAScene.h
			VASceneManager.cpp
			VASceneManager.h
			VASceneState.cpp
			VASceneState.h
			VASceneStateBase.cpp
			VASceneStateBase.h
			VASceneVariables.h
			VASoundSourceDesc.h
			VASoundSourceState.cpp
			VASoundSourceState.h
			VASurfaceState.cpp
			VASurfaceState.h
)

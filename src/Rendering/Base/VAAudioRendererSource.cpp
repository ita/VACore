/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include "VAAudioRendererSource.h"

// VA includes
#include "../../Scene/VASoundSourceDesc.h"

// ITA includes
#include <ITASampleFrame.h>


CVARendererSource::CVARendererSource( const CVABasicMotionModel::Config& oConf_, const CITAVDLConfig& oVDLConf ) : CVARendererMovingObject( oConf_ )
{
	pVDL = std::make_shared<CITASIMOVariableDelayLine>( oVDLConf );
}
void CVARendererSource::PreRequest( )
{
	CVARendererMovingObject::PreRequest( );
	pData = nullptr;
	pVDL->Clear( );
}
void CVARendererSource::PreRelease( )
{
	CVARendererMovingObject::PreRelease( );
}

void CVARendererSource::WriteVDLBlock( )
{
	assert( pData && pData->iID >= 0 );
	const ITASampleFrame& sbInput( *pData->psfSignalSourceInputBuf ); // Always use first channel of SignalSource for VDL
	pVDL->WriteBlock( &( sbInput[0] ) );
}

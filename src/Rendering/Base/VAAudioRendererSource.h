/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VACORE_AUDIORENDERER_SOURCE
#define IW_VACORE_AUDIORENDERER_SOURCE

// VA includes
#include "VAAudioRendererMovingObject.h"
#include "../../Scene/VASoundSourceDesc.h"

#include <ITASIMOVariableDelayLine.h>
#include <memory>

/// Source representation in rendering context
class CVARendererSource : public CVARendererMovingObject
{
private:
	std::shared_ptr<CITASIMOVariableDelayLine> pVDL; // Pointer to SIMO VDL

public:
	CVASoundSourceDesc* pData = nullptr; //!< (Unversioned) Source description

	CVARendererSource( const CVABasicMotionModel::Config& oConf_, const CITAVDLConfig& oVDLConf );
	int GetID( ) const { return pData ? pData->iID : -1; };

	/// Pool-Constructor
	virtual void PreRequest( ) override;
	/// Pool-Destructor
	virtual void PreRelease( ) override;
	

	/// Returns a pointer to the SIMO VDL
	std::shared_ptr<CITASIMOVariableDelayLine> VDL( ) const { return pVDL; };
	/// Takes a new block from the respective signal source and writes it into the VDL
	void WriteVDLBlock( );
};

/// Factory for sources in rendering context
class CVASourcePoolFactory : public IVAPoolObjectFactory
{
public:
	CVASourcePoolFactory( const CVABasicMotionModel::Config& oMotionConf, const CITAVDLConfig& oVDLConf ) : m_oMotionModelConf( oMotionConf ), m_oVDLConf( oVDLConf ) { };

	CVAPoolObject* CreatePoolObject( ) { return new CVARendererSource( m_oMotionModelConf, m_oVDLConf ); };

private:
	const CVABasicMotionModel::Config m_oMotionModelConf;
	const CITAVDLConfig m_oVDLConf;
};

#endif // IW_VACORE_AUDIORENDERER_SOURCE

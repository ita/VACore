target_sources (
	${LIB_TARGET}
	PRIVATE VAAirTrafficNoiseAudioRenderer.cpp
			VAAirTrafficNoiseAudioRenderer.h
			VAATNSourceReceiverTransmission.h
			VAATNSourceReceiverTransmission.cpp
			VAATNSourceReceiverTransmissionHomogeneous.h
			VAATNSourceReceiverTransmissionHomogeneous.cpp
			VAATNSourceReceiverTransmissionInhomogeneous.h
			VAATNSourceReceiverTransmissionInhomogeneous.cpp
			VAAirTrafficNoiseSoundReceiver.h
			# VAAirTrafficNoiseSoundReceiver.cpp
			VAAirTrafficNoiseSoundSource.h
	# VAAirTrafficNoiseSoundSource.cpp
)

/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VACORE_OUTDOORNOISERENDERER
#define IW_VACORE_OUTDOORNOISERENDERER

#include "../Base/VASoundPathRendererBase.h"
#include "../../Filtering/VATemporalVariations.h"

// ITA includes
#include <ITAThirdOctaveMagnitudeSpectrum.h>

// STD includes
#include <map>


/// Sound path based renderer considering a variable number of sound paths including reflections and diffraction, which is ideal for outdoor scenarios.
/// All auralization parameters must be calculated externally and send via SetParameters( )! For example, the calculation can be done using pigeon (image edge model) + ITA-Toolbox.
/// The renderer considers the following acoustic effects:
/// - Spreading loss
/// - Propagation delay / Doppler shift
/// - Source directivity
/// - Air attenuation
/// - Turbulence (temporal variations)
/// - Reflection factor
/// - Diffraction spectrum
/// - Spatial encoding at receiver
/// For details on the actual processing, see CVASoundPathRendererBase.
class CVAOutdoorNoiseRenderer : public CVASoundPathRendererBase
{
public:
	/// Config for OutdoorNoiseRenderer, currently no additional settings.
	struct Config : public CVASoundPathRendererBase::Config
	{

		/// Constructor setting default values
		Config( )
		{
			oExternalSimulation.bTurbulence = true; //Deactivating VA turbulence filter per default
		};
		/// Constructor parsing renderer ini-file parameters. Takes a given config for default values.
		/// Note, that part of the parsing process is done in the parent renderer's config method.
		Config( const CVAAudioRendererInitParams& oParams, const Config& oDefaultValues );

	};

	CVAOutdoorNoiseRenderer( const CVAAudioRendererInitParams& oParams );

private:
	const Config m_oConf; // Renderer configuration parsed from VACore.ini (downcasted copy of child renderer)

	/// Sound paths used by OutdoorNoiseRenderer
	class OutdoorSoundPath : public CVASoundPathRendererBase::SoundPathBase
	{
	protected:
		const Config& m_oConf; // Copy of renderer config

		const std::string sID; // Unique string identifier

		bool bAudible;                                                               // Bool indicating whether sound path is audible or not
		CVAAtmosphericTemporalVariations oATVGenerator;                              // Generator for temporal variation magnitudes caused by turbulence
		ITABase::CThirdOctaveGainMagnitudeSpectrum oTurbulenceMagnitudes;            // Magnitudes for temporal variation due to turbulence
		ITABase::CThirdOctaveGainMagnitudeSpectrum oReflectionDiffractionMagnitudes; // Magnitudes containing reflections and diffraction
		double dDiffrationGain;                                                      // Gain for compensation of missing phase in diffraction filter

	public:
		OutdoorSoundPath( const Config& oConf, CVARendererSource* pSource, CVARendererReceiver* pReceiver, const std::string& sID );
		/// Called during an external auralization parameter update through SetParameters( ).
		/// Calls base function and additionally updates turbulence, reflection and diffraction magnitudes as well as general audibility
		void ExternalUpdate( const CVAStruct& oUpdate );

	protected:
		/// Accumulates the auralization parameters and checks auralization modes.
		/// NOTE: For this renderer, auralization parameters are only changed via SetParameters( ) (see ExternalUpdate( ))
		void UpdateAuralizationParameters( double dTimeStamp, const AuralizationMode& oAuraMode, double& dDelay, double& dSpreadingLoss, VAVec3& v3SourceWavefrontNormal,
		                                   VAVec3& v3ReceiverWavefrontNormal, ITABase::CThirdOctaveGainMagnitudeSpectrum& oAirAttenuationMagnitudes,
		                                   ITABase::CThirdOctaveGainMagnitudeSpectrum& oSoundPathMagnitudes ) override;
	};

	/// Source-Receiver pair of the AirTrafficNoiseRenderer.
	/// Holds a sound path for direct sound and ground reflection, either corresponding to a homogeneous or inhomogeneous medium.
	class SourceReceiverPair : public CVASoundPathRendererBase::SourceReceiverPair
	{
	private:
		const Config& m_oConf;                                                   // Copy of renderer config
		std::map<std::string, std::shared_ptr<OutdoorSoundPath>> m_mpSoundPaths; // Map of sound paths using string identifier keys

	public:
		SourceReceiverPair( const Config& oConf );
		// Pool Destructor, removing all sound paths from map
		virtual void PreRelease( ) override;

		/// Allows an external update of auralization parameters via SetParameters( )
		void ExternalUpdate( const CVAStruct& oParams ) override;

	private:
		bool SoundPathExists( const std::string& sPathID );
		/// Returns the sound path referring to given ID. Creates a new one if required.
		std::shared_ptr<OutdoorSoundPath> GetSoundPath( const std::string& sPathID );
	};

	class SourceReceiverPairFactory : IVAPoolObjectFactory
	{
	private:
		const Config& m_oConf; // Copy of renderer config

	public:
		SourceReceiverPairFactory( const CVAOutdoorNoiseRenderer::Config& oConf )
		    : m_oConf( oConf ) { };
		CVAPoolObject* CreatePoolObject( ) override { return new SourceReceiverPair( m_oConf ); }
	};
};

#endif // IW_VACORE_OUTDOORNOISERENDERER

/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include "VAOutdoorNoiseRenderer.h"


#include "../Base/VAAudioRendererReceiver.h"
#include "../Base/VAAudioRendererSource.h"
#include "../../Utils/VAUtils.h"
#include "../../VALog.h"

#include <utility>


CVAOutdoorNoiseRenderer::Config::Config( const CVAAudioRendererInitParams& oParams, const Config& oDefaultValues )
    : CVASoundPathRendererBase::Config( oParams, oDefaultValues )
{
	CVAConfigInterpreter conf( *oParams.pConfig );
	const std::string sExceptionMsgPrefix = "Renderer ID '" + oParams.sID + "': ";
}



CVAOutdoorNoiseRenderer::CVAOutdoorNoiseRenderer( const CVAAudioRendererInitParams& oParams )
    : CVASoundPathRendererBase( oParams, Config( ) )
    , m_oConf( oParams, Config( ) )
{
	IVAPoolObjectFactory* pFactory = (IVAPoolObjectFactory*)new SourceReceiverPairFactory( m_oConf );
	InitSourceReceiverPairPool( pFactory );
}



CVAOutdoorNoiseRenderer::OutdoorSoundPath::OutdoorSoundPath( const Config& oConf, CVARendererSource* pSource, CVARendererReceiver* pReceiver, const std::string& sID )
    : CVASoundPathRendererBase::SoundPathBase( oConf, pSource, pReceiver )
    , m_oConf( oConf )
    , sID( sID )
    , bAudible( true )
    , dDiffrationGain( 1.0 )
{
	oTurbulenceMagnitudes.SetIdentity( );
	oReflectionDiffractionMagnitudes.SetIdentity( );
}

void CVAOutdoorNoiseRenderer::OutdoorSoundPath::ExternalUpdate( const CVAStruct& oUpdate )
{
	CVASoundPathRendererBase::SoundPathBase::ExternalUpdate( oUpdate );

	if( oUpdate.HasKey( "audible" ) )
		bAudible = oUpdate["audible"];

	if( oUpdate.HasKey( "diffraction_gain" ) )
		dDiffrationGain = oUpdate["diffraction_gain"];

	if( oUpdate.HasKey( "turbulence_third_octaves" ) )
		ParseSpectrum( oUpdate["turbulence_third_octaves"], oTurbulenceMagnitudes );
	else if( oUpdate.HasKey( "temporal_variation_third_octaves" ) )
		ParseSpectrum( oUpdate["temporal_variation_third_octaves"], oTurbulenceMagnitudes );

	if( oUpdate.HasKey( "object_interaction_third_octaves" ) ) //reflection + diffraction
		ParseSpectrum( oUpdate["object_interaction_third_octaves"], oReflectionDiffractionMagnitudes );

	if( oUpdate.HasKey( "reflection_order" ) )
		SetReflectionOrder( oUpdate["reflection_order"] );
	if( oUpdate.HasKey( "diffraction_order" ) )
		SetDiffractionOrder( oUpdate["diffraction_order"] );
}


void CVAOutdoorNoiseRenderer::OutdoorSoundPath::UpdateAuralizationParameters( double dTimeStamp, const AuralizationMode& oAuraMode, double& dDelay, double& dSpreadingLoss,
                                                                             VAVec3& v3SourceWavefrontNormal, VAVec3& v3ReceiverWavefrontNormal,
                                                                             ITABase::CThirdOctaveGainMagnitudeSpectrum& oAirAttenuationMagnitudes,
                                                                             ITABase::CThirdOctaveGainMagnitudeSpectrum& oSoundPathMagnitudes )
{
	dSpreadingLoss *= dDiffrationGain;

	// Limit spreading loss and mute if path is inaudible
	const double dMaxSpreadingLossFactor = 1.0 / m_oConf.oCoreConfig.dDefaultMinimumDistance;
	dSpreadingLoss                       = bAudible ? std::min( dSpreadingLoss, dMaxSpreadingLossFactor ) : 0.0f;

	if( !m_oConf.oExternalSimulation.bTurbulence )
		oATVGenerator.GetMagnitudes( oTurbulenceMagnitudes );

	oSoundPathMagnitudes = oReflectionDiffractionMagnitudes;
	if( oAuraMode.bTemporalVariations )
		oSoundPathMagnitudes *= oTurbulenceMagnitudes;
}



CVAOutdoorNoiseRenderer::SourceReceiverPair::SourceReceiverPair( const Config& oConf )
    : CVASoundPathRendererBase::SourceReceiverPair( oConf )
    , m_oConf( oConf )
{
}

void CVAOutdoorNoiseRenderer::SourceReceiverPair::PreRelease( )
{
	CVASoundPathRendererBase::SourceReceiverPair::PreRelease( );
	m_mpSoundPaths.clear( );
}

void CVAOutdoorNoiseRenderer::SourceReceiverPair::ExternalUpdate( const CVAStruct& oParams )
{
	auto it = oParams.Begin( );
	while( it != oParams.End( ) )
	{
		const CVAStructValue& oPathValue( it++->second );
		//Only structs with 'identifier' keys are processed
		if( !oPathValue.IsStruct( ) )
			continue;
		const CVAStruct& oPath( oPathValue );
		if( !oPath.HasKey( "identifier" ) )
		{
			VA_WARN( "OutdoorNoiseRenderer", "Called SetParameters( ) -> ExternalUpdate( ) with a path missing the 'identifier' key, skipping this path." );
			continue;
		}

		const std::string sPathID = oPath["identifier"];
		std::shared_ptr<OutdoorSoundPath> pSoundPath = GetSoundPath( sPathID );

		pSoundPath->ExternalUpdate( oPath );
	}
}

bool CVAOutdoorNoiseRenderer::SourceReceiverPair::SoundPathExists( const std::string& sPathID )
{
	return m_mpSoundPaths.find( sPathID ) != m_mpSoundPaths.end( );
}

std::shared_ptr<CVAOutdoorNoiseRenderer::OutdoorSoundPath> CVAOutdoorNoiseRenderer::SourceReceiverPair::GetSoundPath( const std::string& sPathID )
{
	if( SoundPathExists( sPathID ) )
		return m_mpSoundPaths[sPathID];

	auto pPath = std::make_shared<OutdoorSoundPath>( m_oConf, pSource, pReceiver, sPathID );
	m_mpSoundPaths[sPathID] = pPath;
	AddSoundPath( pPath ); //Register sound path in SoundPathBaseRenderer

	return pPath;
}

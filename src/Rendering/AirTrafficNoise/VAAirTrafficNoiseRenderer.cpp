/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include "VAAirtrafficNoiseRenderer.h"

#ifdef VACORE_WITH_RENDERER_BINAURAL_AIR_TRAFFIC_NOISE

#include "../../Utils/VAUtils.h"
#include "../../VALog.h"
#include "../Base/VAAudioRendererReceiver.h"
#include "../Base/VAAudioRendererSource.h"

#include <ITA/SimulationScheduler/OutdoorAcoustics/ART/art_simulator.h>
#include <ITAException.h>
#include <ITAGeo/Utils/JSON/Atmosphere.h>
#include <ITAPropagationModels/Atmosphere/AirAttenuation.h>
#include <utility>


//--- CONVERSION FUNCTIONS----

/// Converts a VistaVector3D from ART to OpenGL coordinate system
VistaVector3D ARTtoOpenGLVector( const VistaVector3D& v3RT )
{
	return VistaVector3D( -v3RT[Vista::Y], v3RT[Vista::Z], -v3RT[Vista::X] );
}
/// Converts a VistaVector3D to a VAVec3 without any coordinate transform
VAVec3 VistaToVAVector( const VistaVector3D& v3Vista )
{
	return VAVec3( v3Vista[Vista::X], v3Vista[Vista::Y], v3Vista[Vista::Z] );
}
/// Derives acoustic parameters from ART ray
std::shared_ptr<const CPathProperties> GetARTRayProperties( std::shared_ptr<const CARTRay> pRay, const CStratifiedAtmosphere& oStratifiedAtmosphere )
{
	auto pRayProps                       = std::make_shared<CPathProperties>( );
	pRayProps->dPropagationDelay         = pRay->LastTimeStamp( );
	pRayProps->dSpreadingLoss            = pRay->SpreadingLoss( );
	pRayProps->v3SourceWaveFrontNormal   = ARTtoOpenGLVector( pRay->InitialDirection( ) );
	pRayProps->v3ReceiverWaveFrontNormal = ARTtoOpenGLVector( pRay->LastWavefrontNormal( ) );
	pRayProps->iReflectionOrder          = pRay->ReflectionOrder( );
	pRayProps->sID                       = std::to_string( pRayProps->iReflectionOrder );
	ITAPropagationModels::Atmosphere::AirAttenuationSpectrum( pRayProps->oAirAttenuationSpectrum, *pRay, oStratifiedAtmosphere );

	return pRayProps;
}


void ParseStratifiedAtmosphere( const std::string& sAtmosphereJSONFilepathOrContent, CStratifiedAtmosphere& oAtmosphere )
{
	try
	{
		if( sAtmosphereJSONFilepathOrContent.find( ".json" ) != std::string::npos )
			ITAGeo::Utils::JSON::Import( oAtmosphere, sAtmosphereJSONFilepathOrContent );
		else
			ITAGeo::Utils::JSON::Decode( oAtmosphere, sAtmosphereJSONFilepathOrContent );
	}
	catch( const ITAException& ex )
	{
		VA_EXCEPT2( INVALID_PARAMETER, "Error while parsing stratified atmosphere: " + ex.sReason );
	}
	catch( std::exception& ex )
	{
		VA_EXCEPT2( INVALID_PARAMETER, "Error while parsing stratified atmosphere: " + std::string( ex.what( ) ) );
	}
}


CVAAirTrafficNoiseRenderer::Config::Config( const CVAAudioRendererInitParams& oParams, const Config& oDefaultValues )
    : CVASoundPathRendererBase::Config( oParams, oDefaultValues )
{
	CVAConfigInterpreter conf( *oParams.pConfig );
	const std::string sExceptionMsgPrefix = "Renderer ID '" + oParams.sID + "': ";

	conf.OptString( "MediumType", sMediumType, oDefaultValues.sMediumType );
	conf.OptNumber( "GroundPlanePosition", dGroundPlanePosition, oDefaultValues.dGroundPlanePosition );
	conf.OptString( "StratifiedAtmosphereFilepath", sStratifiedAtmosphereFilepath, oDefaultValues.sStratifiedAtmosphereFilepath );

	const std::string sMediumType_lower = toLowercase( sMediumType );
	if( sMediumType_lower == "homogeneous" )
		eMediumType = EMediumType::Homogeneous;
	else if( sMediumType_lower == "inhomogeneous" )
		eMediumType = EMediumType::Inhomogeneous;
	else
		VA_EXCEPT2( INVALID_PARAMETER, sExceptionMsgPrefix + "Unrecognized medium type '" + sMediumType + "' in configuration" );


	InitDependentParameters( );
}
void CVAAirTrafficNoiseRenderer::Config::InitDependentParameters( )
{
	m_bGeometricSoundPathsRequired =
	    !( oExternalSimulation.bPropagationDelay && oExternalSimulation.bSpreadingLoss && ( oExternalSimulation.bDirectivity || oExternalSimulation.bSourceWFNormal ) &&
	       oExternalSimulation.bReceiverWFNormal && oExternalSimulation.bAirAttenuation );
}


CVAAirTrafficNoiseRenderer::CVAAirTrafficNoiseRenderer( const CVAAudioRendererInitParams& oParams )
    : CVASoundPathRendererBase( oParams, Config( ) )
    , m_oConf( oParams, Config( ) )
{
	if( m_oConf.eMediumType == EMediumType::Inhomogeneous )
	{
		if( !m_oConf.sStratifiedAtmosphereFilepath.empty( ) ) // Otherwise default constructor is used
			ParseStratifiedAtmosphere( m_oConf.sStratifiedAtmosphereFilepath, m_oStratifiedAtmosphere );
	}
	if( !m_oConf.sSimulationSchedulerConfig.empty( ) )
	{
		if( m_oConf.eMediumType != EMediumType::Inhomogeneous )
			VA_EXCEPT2( INVALID_PARAMETER, "Simulation scheduler must be used wih inhomogeneous medium." );

		ITA::SimulationScheduler::CScheduler::LocalSchedulerConfig oLocalSchedConf;
		try
		{
			oLocalSchedConf.Load( ITA::SimulationScheduler::Utils::JSONConfigUtils::LoadJSONConfig( m_oConf.sSimulationSchedulerConfig ) );
			m_pSimulationScheduler = std::make_shared<ITA::SimulationScheduler::CScheduler>( oLocalSchedConf );
			m_pSimulationScheduler->AttachResultHandler( this );
		}
		catch( ITAException& err )
		{
			VA_EXCEPT2( INVALID_PARAMETER, "Error while initializing SimulationScheduler: " + err.ToString( ) );
		}
	}
	
	IVAPoolObjectFactory* pFactory = (IVAPoolObjectFactory*)new SourceReceiverPairFactory( m_oConf, m_oStratifiedAtmosphere, m_pSimulationScheduler );
	InitSourceReceiverPairPool( pFactory );
}

void CVAAirTrafficNoiseRenderer::SetParameters( const CVAStruct& oParams )
{
	CVASoundPathRendererBase::SetParameters( oParams );

	if( oParams.HasKey( "stratified_atmosphere" ) )
	{
		// TODO: Maybe it is necessary to wait for audio context is idle before changing atmosphere
		VA_INFO( m_oParams.sClass, "Updating stratified atmosphere for renderer with ID '" + m_oParams.sID + "' used for inhomogeneous transmission." );
		ParseStratifiedAtmosphere( oParams["stratified_atmosphere"], m_oStratifiedAtmosphere );
	}
}

void CVAAirTrafficNoiseRenderer::PostResultReceived( std::unique_ptr<ITA::SimulationScheduler::CSimulationResult> pResult )
{
	const auto pOutdoorSimResult = dynamic_cast<ITA::SimulationScheduler::OutdoorAcoustics::COutdoorSimulationResult*>( pResult.get( ) );
	if( !pOutdoorSimResult )
		return;

	const int iSourceID              = pOutdoorSimResult->sourceReceiverPair.source->GetId( );
	const int iReceiverID            = pOutdoorSimResult->sourceReceiverPair.receiver->GetId( );
	const double& dResultRequestTime = pOutdoorSimResult->dTimeStamp;

	auto pSRPair = dynamic_cast<CVAAirTrafficNoiseRenderer::SourceReceiverPair*>( GetSourceReceiverPair( iSourceID, iReceiverID ) );
	if( !pSRPair )
		VA_EXCEPT2( INVALID_PARAMETER, "Could not find source-receiver pair for given simulation data." );


	if( pOutdoorSimResult->voPathProperties.size( ) < 2 )
		VA_EXCEPT2( INVALID_PARAMETER, "ART simulation returned less than 2 rays!" );

	pOutdoorSimResult->voPathProperties[0].v3SourceWaveFrontNormal = ARTtoOpenGLVector( pOutdoorSimResult->voPathProperties[0].v3SourceWaveFrontNormal );
	pOutdoorSimResult->voPathProperties[0].v3ReceiverWaveFrontNormal = ARTtoOpenGLVector( pOutdoorSimResult->voPathProperties[0].v3ReceiverWaveFrontNormal );
	pOutdoorSimResult->voPathProperties[1].v3SourceWaveFrontNormal   = ARTtoOpenGLVector( pOutdoorSimResult->voPathProperties[1].v3SourceWaveFrontNormal );
	pOutdoorSimResult->voPathProperties[1].v3ReceiverWaveFrontNormal = ARTtoOpenGLVector( pOutdoorSimResult->voPathProperties[1].v3ReceiverWaveFrontNormal );

	// TODO: Maybe it would be good to store individual path result as shared pointers within simulation scheduler?
	pSRPair->UpdatePathProperties( dResultRequestTime, std::make_shared<const CPathProperties>( pOutdoorSimResult->voPathProperties[0] ),
	                               std::make_shared<const CPathProperties>( pOutdoorSimResult->voPathProperties[1] ) );

	VA_TRACE( m_oParams.sClass, "Received simulation scheduler result." );
}


CVAAirTrafficNoiseRenderer::ATNSoundPath::ATNSoundPath( const Config& oConf, CVARendererSource* pSource, CVARendererReceiver* pReceiver )
    : CVASoundPathRendererBase::SoundPathBase( oConf, pSource, pReceiver )
    , m_oConf( oConf )
{
	oTurbulenceMagnitudes.SetIdentity( );
	oGroundReflectionMagnitudes = oConf.oDefaultReflectionFactor;
}

void CVAAirTrafficNoiseRenderer::ATNSoundPath::HomogeneousSimulation( double dSimulationTime )
{
	VAVec3 v3SourcePos = GetSourcePos( );
	if( ReflectionOrder( ) ) // Image source for ground reflection
		v3SourcePos.y = m_oConf.dGroundPlanePosition - ( v3SourcePos.y - m_oConf.dGroundPlanePosition );

	const VAVec3 v3SourceToReceiver = GetReceiverPos( ) - v3SourcePos;
	const double dDistance          = v3SourceToReceiver.Length( );
	if( dDistance == 0.0 )
		return;

	auto pProperties                       = std::make_shared<CPathProperties>( );
	pProperties->dPropagationDelay         = dDistance / m_oConf.oHomogeneousMedium.dSoundSpeed;
	pProperties->dSpreadingLoss            = 1.0 / dDistance;
	const VAVec3 v3WFNormal                = v3SourceToReceiver / dDistance;
	pProperties->v3SourceWaveFrontNormal   = VistaVector3D( v3WFNormal.comp ); // TODO: This conversion is not nice, but therefore we save a conversion when using ART
	pProperties->v3ReceiverWaveFrontNormal = VistaVector3D( v3WFNormal.comp );
	HomogeneousAirAbsorption( dDistance, m_oConf.oHomogeneousMedium, pProperties->oAirAttenuationSpectrum );

	UpdatePathProperties( dSimulationTime, pProperties );
}

void CVAAirTrafficNoiseRenderer::ATNSoundPath::ExternalUpdate( const CVAStruct& oUpdate )
{
	CVASoundPathRendererBase::SoundPathBase::ExternalUpdate( oUpdate );

	if( oUpdate.HasKey( "turbulence_third_octaves" ) )
		ParseSpectrum( oUpdate["turbulence_third_octaves"], oTurbulenceMagnitudes );
	else if( oUpdate.HasKey( "temporal_variation_third_octaves" ) )
		ParseSpectrum( oUpdate["temporal_variation_third_octaves"], oTurbulenceMagnitudes );
	if( oUpdate.HasKey( "ground_reflection_third_octaves" ) )
		ParseSpectrum( oUpdate["ground_reflection_third_octaves"], oGroundReflectionMagnitudes );
}


void CVAAirTrafficNoiseRenderer::ATNSoundPath::UpdateAuralizationParameters( double dTimeStamp, const AuralizationMode& oAuraMode, double& dDelay, double& dSpreadingLoss,
                                                                             VAVec3& v3SourceWavefrontNormal, VAVec3& v3ReceiverWavefrontNormal,
                                                                             ITABase::CThirdOctaveGainMagnitudeSpectrum& oAirAttenuationMagnitudes,
                                                                             ITABase::CThirdOctaveGainMagnitudeSpectrum& oSoundPathMagnitudes )
{
	if( m_oConf.GeometricSoundPathsRequired( ) )
		ApplyAuralizationParameters( dTimeStamp, dDelay, dSpreadingLoss, v3SourceWavefrontNormal, v3ReceiverWavefrontNormal, oAirAttenuationMagnitudes );

	// Limiting spreading loss
	const double dMaxSpreadingLossFactor = 1.0 / m_oConf.oCoreConfig.dDefaultMinimumDistance;
	dSpreadingLoss                       = std::min( dSpreadingLoss, dMaxSpreadingLossFactor );

	if( !m_oConf.oExternalSimulation.bTurbulence )
		oATVGenerator.GetMagnitudes( oTurbulenceMagnitudes );

	//// TODO: Add a way to set reflection without SetParameters?
	// if( ReflectionOrder( ) && !m_oConf.bReflectionExternalSimulation )
	//{
	//	//oGroundReflectionMagnitudes = ...
	//}

	oSoundPathMagnitudes.SetIdentity( );
	if( oAuraMode.bTemporalVariations )
		oSoundPathMagnitudes *= oTurbulenceMagnitudes;
	if( ReflectionOrder( ) && oAuraMode.bEarlyReflections )
		oSoundPathMagnitudes *= oGroundReflectionMagnitudes;
}

void CVAAirTrafficNoiseRenderer::ATNSoundPath::ApplyAuralizationParameters( double dTimeStamp, double& dDelay, double& dSpreadingLoss, VAVec3& v3SourceWavefrontNormal,
                                                                            VAVec3& v3ReceiverWavefrontNormal,
                                                                            ITABase::CThirdOctaveGainMagnitudeSpectrum& oAirAttenuationMagnitudes )
{
	assert( m_pPathProperties );

	if( !m_oConf.oExternalSimulation.bPropagationDelay )
		dDelay = m_pPathProperties->dPropagationDelay;
	if( !m_oConf.oExternalSimulation.bSpreadingLoss )
		dSpreadingLoss = m_pPathProperties->dSpreadingLoss;
	if( !m_oConf.oExternalSimulation.bSourceWFNormal )
		v3SourceWavefrontNormal = VistaToVAVector( m_pPathProperties->v3SourceWaveFrontNormal );
	if( !m_oConf.oExternalSimulation.bReceiverWFNormal )
		v3ReceiverWavefrontNormal = VistaToVAVector( m_pPathProperties->v3ReceiverWaveFrontNormal );
	if( !m_oConf.oExternalSimulation.bAirAttenuation )
		oAirAttenuationMagnitudes = m_pPathProperties->oAirAttenuationSpectrum;

	m_pPathProperties = nullptr;
}

CVAAirTrafficNoiseRenderer::ATNSoundPathWithHistories::ATNSoundPathWithHistories( const Config& oConf, CVARendererSource* pSource, CVARendererReceiver* pReceiver )
    : ATNSoundPath( oConf, pSource, pReceiver )
    , oDelayHistory( oConf.oPropagationParameterHistory.oGeneral, oConf.oPropagationParameterHistory.oPropagationDelayMethod )
    , oSpreadingLossGainHistory( oConf.oPropagationParameterHistory.oGeneral, oConf.oPropagationParameterHistory.oSpreadingLossMethod )
    , oAirAttenuationHistory( oConf.oPropagationParameterHistory.oGeneral.iBufferSize, oConf.oPropagationParameterHistory.oAirAttenuationMethod ) //PSC: Currently no logging implemented for 1/3 oct. magnitudes, thus only handing buffersize
    , oSourceWFHistory( oConf.oPropagationParameterHistory.oGeneral, oConf.oPropagationParameterHistory.oLaunchDirectionMethod)
    , oReceiverWFHistory( oConf.oPropagationParameterHistory.oGeneral, oConf.oPropagationParameterHistory.oIncidentDirectionMethod )
{
}

void CVAAirTrafficNoiseRenderer::ATNSoundPathWithHistories::UpdatePathProperties( double dSimulationTime, std::shared_ptr<const CPathProperties> pPathProperties )
{
	const double dReceiverTime = dSimulationTime + pPathProperties->dPropagationDelay;
	oDelayHistory.Push( dReceiverTime, std::make_unique<double>( pPathProperties->dPropagationDelay ) );
	oSpreadingLossGainHistory.Push( dReceiverTime, std::make_unique<double>( pPathProperties->dSpreadingLoss ) );
	oAirAttenuationHistory.Push( dReceiverTime, std::make_unique<ITABase::CThirdOctaveFactorMagnitudeSpectrum>( pPathProperties->oAirAttenuationSpectrum ) );
	oSourceWFHistory.Push( dReceiverTime, std::make_unique<VAVec3>( VistaToVAVector( pPathProperties->v3SourceWaveFrontNormal ) ) );
	oReceiverWFHistory.Push( dReceiverTime, std::make_unique<VAVec3>( VistaToVAVector( pPathProperties->v3ReceiverWaveFrontNormal ) ) );
}

void CVAAirTrafficNoiseRenderer::ATNSoundPathWithHistories::ApplyAuralizationParameters( double dTimeStamp, double& dDelay, double& dSpreadingLoss,
                                                                                         VAVec3& v3SourceWavefrontNormal, VAVec3& v3ReceiverWavefrontNormal,
                                                                                         ITABase::CThirdOctaveGainMagnitudeSpectrum& oAirAttenuationMagnitudes )
{
	oDelayHistory.Update( dTimeStamp );
	oSpreadingLossGainHistory.Update( dTimeStamp );
	oAirAttenuationHistory.Update( dTimeStamp );
	oSourceWFHistory.Update( dTimeStamp );
	oReceiverWFHistory.Update( dTimeStamp );

	dDelay = 2.0; //TODO: Do not render if no data available yet!
	if( !m_oConf.oExternalSimulation.bPropagationDelay )
		oDelayHistory.Estimate( dTimeStamp, dDelay );
	if( !m_oConf.oExternalSimulation.bSpreadingLoss )
		oSpreadingLossGainHistory.Estimate( dTimeStamp, dSpreadingLoss );
	if( !m_oConf.oExternalSimulation.bSourceWFNormal )
		oSourceWFHistory.Estimate( dTimeStamp, v3SourceWavefrontNormal );
	if( !m_oConf.oExternalSimulation.bReceiverWFNormal )
		oReceiverWFHistory.Estimate( dTimeStamp, v3ReceiverWavefrontNormal );
	if( !m_oConf.oExternalSimulation.bAirAttenuation )
		oAirAttenuationHistory.Estimate( dTimeStamp, oAirAttenuationMagnitudes );
}


CVAAirTrafficNoiseRenderer::SourceReceiverPair::SourceReceiverPair( const Config& oConf, const CStratifiedAtmosphere& oAtmos,
                                                                    std::shared_ptr<ITA::SimulationScheduler::CScheduler> pSimulationScheduler )
    : CVASoundPathRendererBase::SourceReceiverPair( oConf )
    , m_oConf( oConf )
    , m_oStratifiedAtmosphere( oAtmos )
    , m_pSimulationScheduler( pSimulationScheduler )
{
	m_oARTEngine.eigenraySettings.rayTracing.maxTime                       = 60;
	m_oARTEngine.eigenraySettings.rayAdaptation.advancedRayZooming.bActive = false;
	m_oARTEngine.simulationSettings.bMultiThreading                        = false;
}

void CVAAirTrafficNoiseRenderer::SourceReceiverPair::InitSourceAndReceiver( CVARendererSource* pSource_, CVARendererReceiver* pReceiver_ )
{
	CVASoundPathRendererBase::SourceReceiverPair::InitSourceAndReceiver( pSource_, pReceiver_ );

	std::shared_ptr<SoundPathBase> pDirectPath;
	std::shared_ptr<SoundPathBase> pReflectedPath;
	if( m_oConf.oPropagationParameterHistory.bEnabled )
	{
		pDirectPath    = std::make_shared<ATNSoundPathWithHistories>( m_oConf, pSource, pReceiver );
		pReflectedPath = std::make_shared<ATNSoundPathWithHistories>( m_oConf, pSource, pReceiver );
	}
	else
	{
		pDirectPath    = std::make_shared<ATNSoundPath>( m_oConf, pSource, pReceiver );
		pReflectedPath = std::make_shared<ATNSoundPath>( m_oConf, pSource, pReceiver );
	}

	pReflectedPath->SetReflectionOrder( 1 );
	AddSoundPath( pDirectPath );
	AddSoundPath( pReflectedPath );
}

VistaVector3D CVAAirTrafficNoiseRenderer::SourceReceiverPair::VAtoARTCoords( const VAVec3& v3VA )
{
	return VistaVector3D( -v3VA.z, -v3VA.x, v3VA.y - m_oConf.dGroundPlanePosition );
}

VAVec3 CVAAirTrafficNoiseRenderer::SourceReceiverPair::ARTtoVACoords( const VistaVector3D& v3RT )
{
	return VAVec3( -v3RT[Vista::Y], v3RT[Vista::Z] + m_oConf.dGroundPlanePosition, -v3RT[Vista::X] );
}

void CVAAirTrafficNoiseRenderer::SourceReceiverPair::RunSimulation( double dRequestTime )
{
	if( !m_oConf.GeometricSoundPathsRequired( ) )
		return;

	if( m_oConf.eMediumType == EMediumType::Inhomogeneous )
	{
		const VistaVector3D v3SourcePos   = VAtoARTCoords( pSource->PredictedPosition( ) );
		const VistaVector3D v3ReceiverPos = VAtoARTCoords( pReceiver->PredictedPosition( ) );
		if( !m_pSimulationScheduler ) // Sequential simulation
		{
			std::vector<std::shared_ptr<CARTRay>> vpEigenrays = m_oARTEngine.Run( m_oStratifiedAtmosphere, v3SourcePos, v3ReceiverPos );
			if( vpEigenrays.size( ) < 2 )
				VA_EXCEPT2( INVALID_PARAMETER, "ART simulation returned less than 2 rays!" );

			std::shared_ptr<const CPathProperties> pDirectRayProperties    = GetARTRayProperties( vpEigenrays[0], m_oStratifiedAtmosphere );
			std::shared_ptr<const CPathProperties> pReflectedRayProperties = GetARTRayProperties( vpEigenrays[1], m_oStratifiedAtmosphere );

			UpdatePathProperties( dRequestTime, pDirectRayProperties, pReflectedRayProperties );
		}
		else // Scheduled simulation
		{
			VistaQuaternion qOrient = VistaQuaternion( 1, 0, 0, 0 ); // Irrelevant to ART simulation but necessary on C3DObject constructor
			auto pSourceObject =
			    std::make_unique<ITA::SimulationScheduler::C3DObject>( v3SourcePos, qOrient, ITA::SimulationScheduler::C3DObject::Type::source, pSource->GetID( ) );
			auto pReceiverObject =
			    std::make_unique<ITA::SimulationScheduler::C3DObject>( v3ReceiverPos, qOrient, ITA::SimulationScheduler::C3DObject::Type::receiver, pReceiver->GetID( ) );

			auto pUpdate = std::make_unique<ITA::SimulationScheduler::CUpdateScene>( dRequestTime );
			pUpdate->SetSourceReceiverPair( std::move( pSourceObject ), std::move( pReceiverObject ) );

			m_pSimulationScheduler->PushUpdate( std::move( pUpdate ) );
		}
	}
	else // Homogeneous medium
	{
		for( auto pSoundPath: GetSoundPaths( ) )
		{
			auto pATNSoundPath = std::dynamic_pointer_cast<ATNSoundPath>( pSoundPath );
			assert( pATNSoundPath );
			pATNSoundPath->HomogeneousSimulation( dRequestTime );
		}
	}
}

void CVAAirTrafficNoiseRenderer::SourceReceiverPair::UpdatePathProperties( double dSimulationTime, std::shared_ptr<const CPathProperties> pDirectPathProperties,
                                                                           std::shared_ptr<const CPathProperties> pReflectedPathProperties )
{
	for( auto pSoundPath: GetSoundPaths( ) )
	{
		auto pATNSoundPath = std::dynamic_pointer_cast<ATNSoundPath>( pSoundPath );
		assert( pATNSoundPath );

		if( pATNSoundPath->ReflectionOrder( ) )
			pATNSoundPath->UpdatePathProperties( dSimulationTime, pReflectedPathProperties );
		else
			pATNSoundPath->UpdatePathProperties( dSimulationTime, pDirectPathProperties );
	}
}

void CVAAirTrafficNoiseRenderer::SourceReceiverPair::ExternalUpdate( const CVAStruct& oParams )
{
	assert( GetSoundPaths( ).size( ) == 2 );

	auto pDirectPath    = std::dynamic_pointer_cast<ATNSoundPath>( GetSoundPaths( ).front( ) );
	auto pReflectedPath = std::dynamic_pointer_cast<ATNSoundPath>( GetSoundPaths( ).back( ) );

	if( oParams.HasKey( "direct_path" ) )
		pDirectPath->ExternalUpdate( oParams["direct_path"] );
	if( oParams.HasKey( "reflected_path" ) )
		pReflectedPath->ExternalUpdate( oParams["reflected_path"] );
}


#endif // VACORE_WITH_RENDERER_BINAURAL_AIR_TRAFFIC_NOISE
/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VACORE_AIRTRAFFICNOISERENDERER
#define IW_VACORE_AIRTRAFFICNOISERENDERER

#ifdef VACORE_WITH_RENDERER_BINAURAL_AIR_TRAFFIC_NOISE

//clang-format off
// Some header seems to include some part of the windows header before VistaTicker gets to.
// Todo: this is just a quick fix and should be removed as soon as possible.
#include <VistaInterProcComm/Concurrency/VistaTicker.h>
//clang-format on

#include "../../DataHistory/VADoubleHistoryModel.h"
#include "../../DataHistory/VASpectrumHistoryModel.h"
#include "../../DataHistory/VAVec3HistoryModel.h"
#include "../../Filtering/VATemporalVariations.h"
#include "../Base/VASoundPathRendererBase.h"

// ITA includes
#include <ITAThirdOctaveMagnitudeSpectrum.h>

// ART includes
#include <ITAGeo/Atmosphere/StratifiedAtmosphere.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/EigenraySearch/EigenrayEngine.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Rays.h>
#include <VistaBase/VistaVector3D.h>

// ITA simulation scheduler includes
#include <ITA/SimulationScheduler/result_handler.h>
#include <ITA/SimulationScheduler/scheduler.h>
#include <ITA/SimulationScheduler/OutdoorAcoustics/outdoor_simulation_result.h>


typedef ITAPropagationPathSim::AtmosphericRayTracing::EigenraySearch::CEngine CARTEngine;
typedef ITAGeo::CStratifiedAtmosphere CStratifiedAtmosphere;
typedef ITAPropagationPathSim::AtmosphericRayTracing::CRay CARTRay;
typedef ITA::SimulationScheduler::OutdoorAcoustics::COutdoorSimulationResult::CPathProperty CPathProperties;


/// Sound path based renderer considering direct sound and a single reflection assuming a flat ground which are reasonable assumptions for aircraft flyovers.
/// Either uses straight paths based on image source method (homogeneous medium) or curved sound paths based on the Atmospheric Ray Tracing method (stratified, moving medium).
/// The renderer considers the following acoustic effects:
/// - Spreading loss
/// - Propagation delay / Doppler shift
/// - Source directivity
/// - Air attenuation
/// - Turbulence (temporal variations)
/// - Reflection factor (only for ground reflection)
/// - Spatial encoding at receiver
/// For details on the actual processing, see CVASoundPathRendererBase.
class CVAAirTrafficNoiseRenderer
    : public CVASoundPathRendererBase
    , public ITA::SimulationScheduler::IResultHandler
{
public:
	/// Enum to switch between homogeneous and inhomogeneous medium
	enum class EMediumType
	{
		Homogeneous   = 0, /// A homogeneous medium with straight sound paths
		Inhomogeneous = 1  /// Stratified, moving medium with curved sound paths based on ART framework
	};
	/// Config for AirTrafficNoiseRenderer, allows specifing the medium and ground plane altitude.
	struct Config : public CVASoundPathRendererBase::Config
	{
		EMediumType eMediumType                   = EMediumType::Homogeneous; /// Medium type used for sound path simulations
		std::string sMediumType                   = "homogeneous";            /// String representing medium type in .ini file
		std::string sStratifiedAtmosphereFilepath = "";                       /// Filepath to load a StratifiedAtmosphere configuration from a json file (empty = default)
		double dGroundPlanePosition               = 0.0;                      /// Altitude-position of ground plane (y = ...)

		/// Constructor setting default values
		Config( )
		{
			sVDLSwitchingAlgorithm          = "cubicspline";
			sFilterBankType                 = "iir_biquads_order10";
			InitDependentParameters( );
		};
		/// Constructor parsing renderer ini-file parameters. Takes a given config for default values.
		/// Note, that part of the parsing process is done in the parent renderer's config method.
		Config( const CVAAudioRendererInitParams& oParams, const Config& oDefaultValues );

		/// Returns true, if geometric paths are not required, since all respective auralization parameters are set externally
		bool GeometricSoundPathsRequired( ) const { return m_bGeometricSoundPathsRequired; };

	private:
		bool m_bGeometricSoundPathsRequired = true;
		void InitDependentParameters( );
	};

	CVAAirTrafficNoiseRenderer( const CVAAudioRendererInitParams& oParams );

	/// Handles a set rendering module parameters call()
	/// Calls sounpath-base renderer function. Additionally parses stratified medium .json file.
	virtual void SetParameters( const CVAStruct& oParams ) override;
	/// Handles an ART simulation result being returned from the simulation scheduler
	void PostResultReceived( std::unique_ptr<ITA::SimulationScheduler::CSimulationResult> pResult ) override;

private:
	const Config m_oConf;                                                         /// Renderer configuration parsed from VACore.ini (downcasted copy of child renderer)
	CStratifiedAtmosphere m_oStratifiedAtmosphere;                                /// Stratified atmosphere used if medium type is inhomogeneous
	std::shared_ptr<ITA::SimulationScheduler::CScheduler> m_pSimulationScheduler; /// Pointer to simulation scheduler

	/// Base class for sound paths of the AirTrafficNoiseRenderer without using Histories for auralization parameters
	class ATNSoundPath : public CVASoundPathRendererBase::SoundPathBase
	{
	protected:
		const Config& m_oConf;                                              /// Copy of renderer config
		std::shared_ptr<const CPathProperties> m_pPathProperties = nullptr; /// Acoustic properties of this sound path taken from simulation

		CVAAtmosphericTemporalVariations oATVGenerator;                         /// Generator for temporal variation magnitudes caused by turbulence
		ITABase::CThirdOctaveGainMagnitudeSpectrum oTurbulenceMagnitudes;       /// Magnitudes for temporal variation due to turbulence
		ITABase::CThirdOctaveGainMagnitudeSpectrum oGroundReflectionMagnitudes; /// Ground reflection magnitudes (only for reflected path)

	public:
		ATNSoundPath( const Config& oConf, CVARendererSource* pSource, CVARendererReceiver* pReceiver );

		void HomogeneousSimulation( double dSimulationTime );
		/// Stores the given acoustic properties directly as is (without histories)
		virtual void UpdatePathProperties( double dSimulationTime, std::shared_ptr<const CPathProperties> pPathProperties ) { m_pPathProperties = pPathProperties; };

		/// Called during an external auralization parameter update.
		/// Calls base function and additionally updates air attenuation, turbulence and ground reflection magnitudes
		void ExternalUpdate( const CVAStruct& oUpdate );

	protected:
		void UpdateAuralizationParameters( double dTimeStamp, const AuralizationMode& oAuraMode, double& dDelay, double& dSpreadingLoss, VAVec3& v3SourceWavefrontNormal,
		                                   VAVec3& v3ReceiverWavefrontNormal, ITABase::CThirdOctaveGainMagnitudeSpectrum& oAirAttenuationMagnitudes,
		                                   ITABase::CThirdOctaveGainMagnitudeSpectrum& oSoundPathMagnitudes ) override;
		/// Applies path properties to auralization parameters directly (without histories)
		virtual void ApplyAuralizationParameters( double dTimeStamp, double& dDelay, double& dSpreadingLoss, VAVec3& v3SourceWavefrontNormal,
		                                          VAVec3& v3ReceiverWavefrontNormal, ITABase::CThirdOctaveGainMagnitudeSpectrum& oAirAttenuationMagnitudes );
	};
	/// Sound path using histories for auralization parameters
	class ATNSoundPathWithHistories : public ATNSoundPath
	{
	private:
		CVADoubleHistoryModel oDelayHistory;             ///< History buffer storing doubles with propagation delays
		CVADoubleHistoryModel oSpreadingLossGainHistory; ///< History buffer storing doubles of spreading loss gain
		CVASpectrumHistoryModel oAirAttenuationHistory;  ///< History buffer storing third-octave air attenuation magnitude spectra
		CVAVec3HistoryModel oSourceWFHistory;            ///< History buffer storing wavefront normal at source
		CVAVec3HistoryModel oReceiverWFHistory;          ///< History buffer storing the wavefront normal at receiver
	public:
		ATNSoundPathWithHistories( const Config& oConf, CVARendererSource* pSource, CVARendererReceiver* pReceiver );

		/// Pushes the given acoustic properties to respective auralization parameter histories
		void UpdatePathProperties( double dSimulationTime, std::shared_ptr<const CPathProperties> pPathProperties ) override;

	protected:
		/// Estimates current auralization parameters based on current time stamp using the respective histories
		void ApplyAuralizationParameters( double dTimeStamp, double& dDelay, double& dSpreadingLoss, VAVec3& v3SourceWavefrontNormal, VAVec3& v3ReceiverWavefrontNormal,
		                                  ITABase::CThirdOctaveGainMagnitudeSpectrum& oAirAttenuationMagnitudes ) override;
	};

	/// Source-Receiver pair of the AirTrafficNoiseRenderer.
	/// Holds a sound path for direct sound and ground reflection, either corresponding to a homogeneous or inhomogeneous medium.
	class SourceReceiverPair : public CVASoundPathRendererBase::SourceReceiverPair
	{
	private:
		const Config& m_oConf;                                                        /// Copy of renderer config
		const CStratifiedAtmosphere& m_oStratifiedAtmosphere;                         /// Copy of renderer atmosphere
		std::shared_ptr<ITA::SimulationScheduler::CScheduler> m_pSimulationScheduler; /// Pointer to simulation scheduler

		CARTEngine m_oARTEngine; /// ART simulation engine

		VistaVector3D VAtoARTCoords( const VAVec3& v3VA );
		VAVec3 ARTtoVACoords( const VistaVector3D& v3RT );

		/// Runs an ART simulation if using inhomogeneous medium
		void RunSimulation( double dRequestTime ) override;

	public:
		SourceReceiverPair( const Config& oConf, const CStratifiedAtmosphere& oAtmos, std::shared_ptr<ITA::SimulationScheduler::CScheduler> pSimulationScheduler );
		void InitSourceAndReceiver( CVARendererSource* pSource_, CVARendererReceiver* pReceiver_ ) override;

		/// Feeds the simulation result into the auralization parameter histories of the sound paths
		void UpdatePathProperties( double dSimulationTime, std::shared_ptr<const CPathProperties> pDirectPathProperties,
		                           std::shared_ptr<const CPathProperties> pReflectedPathProperties );
		void ExternalUpdate( const CVAStruct& oParams ) override;
	};

	class SourceReceiverPairFactory : IVAPoolObjectFactory
	{
	private:
		const Config& m_oConf;                                                        /// Copy of renderer config
		const CStratifiedAtmosphere& m_oStratifiedAtmosphere;                         /// Copy of renderer atmosphere
		std::shared_ptr<ITA::SimulationScheduler::CScheduler> m_pSimulationScheduler; /// Pointer to simulation scheduler

	public:
		SourceReceiverPairFactory( const CVAAirTrafficNoiseRenderer::Config& oConf, const CStratifiedAtmosphere& oAtmos,
		                           std::shared_ptr<ITA::SimulationScheduler::CScheduler> pSimulationScheduler )
		    : m_oConf( oConf )
		    , m_oStratifiedAtmosphere( oAtmos )
		    , m_pSimulationScheduler( pSimulationScheduler ) { };
		CVAPoolObject* CreatePoolObject( ) override { return new SourceReceiverPair( m_oConf, m_oStratifiedAtmosphere, m_pSimulationScheduler ); }
	};
};

#endif // VACORE_WITH_RENDERER_BINAURAL_AIR_TRAFFIC_NOISE

#endif // IW_VACORE_AIRTRAFFICNOISERENDERER

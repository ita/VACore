/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VACORE_LOG
#define IW_VACORE_LOG

#include <ITAClock.h>
#include <VA.h>
#include <VACoreDefinitions.h>
#include <atomic>
#include <iomanip>
#include <iostream>
#include <list>
#include <stdarg.h>
#include <stdio.h>
#include <tbb/concurrent_queue.h>

// These Vista includes must be done before spdlog gets included due to weird windows.h inclusion
#include <VistaInterProcComm/Concurrency/VistaMutex.h>
#include <VistaInterProcComm/Concurrency/VistaThread.h>
#include <VistaInterProcComm/Concurrency/VistaThreadEvent.h>
#include <VistaInterProcComm/Concurrency/VistaThreadLoop.h>
#include <VistaInterProcComm/Concurrency/VistaTicker.h>

//clang-format off
#include "VACore_instrumentation.h"
#include "spdlog/fmt/bundled/core.h"
#include "spdlog/fmt/bundled/ostream.h"
//clang-format on

int VACORE_API VALog_GetLogLevel( );
void VACORE_API VALog_SetLogLevel( int );


namespace detail
{
	template<typename... Args>
	std::string log_format_helper( spdlog::format_string_t<Args...> fmt, Args &&...args )
	{
		return fmt::format( fmt, std::forward<Args>( args )... );
	}

	template<typename T>
	std::string log_format_helper( const T &msg )
	{
		return fmt::format( "{}", msg );
	}
} // namespace detail

#define VA_CRITICAL( module, ... ) VACORE_CRITICAL( "[{:<20}] {}", module, detail::log_format_helper( __VA_ARGS__ ) )
#define VA_ERROR( module, ... )    VACORE_ERROR( "[{:<20}] {}", module, detail::log_format_helper( __VA_ARGS__ ) )
#define VA_WARN( module, ... )     VACORE_WARN( "[{:<20}] {}", module, detail::log_format_helper( __VA_ARGS__ ) )
#define VA_INFO( module, ... )     VACORE_INFO( "[{:<20}] {}", module, detail::log_format_helper( __VA_ARGS__ ) )
#define VA_VERBOSE( module, ... )  VACORE_DEBUG( "[{:<20}] {}", module, detail::log_format_helper( __VA_ARGS__ ) )
#define VA_TRACE( module, ... )    VACORE_TRACE( "[{:<20}] {}", module, detail::log_format_helper( __VA_ARGS__ ) )

#define VA_PRINT( ... ) VACORE_INFO( "[{:<20}] {}", "VAPrint", detail::log_format_helper( __VA_ARGS__ ) )

#endif // IW_VACORE_LOG

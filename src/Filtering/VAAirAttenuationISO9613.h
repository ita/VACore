/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VACORE_AIRATTENUATION_ISO9613
#define IW_VACORE_AIRATTENUATION_ISO9613

#include "../Medium/VAHomogeneousMedium.h"

#include <ITAISO9613.h>
#include <ITAThirdOctaveMagnitudeSpectrum.h>

namespace VA
{
	//Calculates the air attenuation magnitudes using ISO 9613-1 for given source-receiver distance and homogeneous medium and stores it in given 1/3-octave band 
	static void AirAttenuationISO9613( ITABase::CThirdOctaveGainMagnitudeSpectrum& oOutputSpectrum, double dDistance, const CVAHomogeneousMedium& oMedium )
	{
		const double& dTemperature = oMedium.dTemperatureDegreeCentigrade;
		const double& dHumidity    = oMedium.dRelativeHumidityPercent;
		const double dPressureKPa  = oMedium.dStaticPressurePascal / 1000; //-> [kPa]

		ITABase::ISO9613::AtmosphericAbsorption( oOutputSpectrum, dDistance, dTemperature, dHumidity, dPressureKPa );
	};
} // namespace VA

#endif // IW_VACORE_AIRATTENUATION_ISO9613

# VA common data

## License

#### CC BY 4.0
The following files are licensed under Creative Commons BY 4.0
 - WelcomeToVA.wav
 - AircraftCruise.wav
 - AmbientOutdoorSound.wav
 - Electric_AVAS_3_5_5sec.wav
 - TyreNoise_front.wav
 - TyreNoise_rear.wav
 - TyreNoise.daff
 - SimpleTJunction.skp

#### CC BY-NC-SA  4.0
The following files are licensed under Creative Commons BY-NC-SA 4.0 by the Institute for Hearing Technology and Acoustics (IHTA), RWTH Aachen University:
 - ITA_Artificial_Head_5x5_44kHz_128.v17.ir.daff
 - Singer.v17.ms.daff
 - Trumpet1.v17.ms.daff
 - HD650_all_inv.wav
 - ambeo_rir_ita_doorway.wav
 
For more information, higher resolutions for academic purposes and commercial use, please contact us.

#### External files
The following file is taken from the [EduRa database](https://doi.org/10.18154/RWTH-2021-07429) but having a reduced gain:
- EduRa_Cluster2_A-BRIR_R1S8_gain_-18dB.wav

## References
#### Aircraft noise signal
This signal (`AircraftCruise.wav`) combines a model for fan and jet noise. It is generated for an aircraft flying at constant velocity of approximately 300 km/h. The respective references are:
- Fan noise: https://doi.org/10.3397/IN-2021-1742
- Jet noise: https://doi.org/10.1121/10.0020065

#### Tyre noise
The road-tyre noise signals and directivity (`TyreNoise_front.wav`, `TyreNoise_rear.wav` and `TyreNoise.daff`) are based on the model(s) published in:
*Dreier, C. and Hahn, J. and Vorländer, M.: Inverse modelling of vehicle pass-by noise for auralizations. In: Proc. DAGA – Fortschritte der Akustik, 48:1455–1458, 2022*

#### AVAS signal
The signal `Electric_AVAS_3_5_5sec.wav` represents the Acoustic Vehicle Alert System (AVAS) sound of an electric car with the following trajectory
- 3s at constant velocity 10 km/h
- 5s constant acceleration up to 50 km/h
- 5s at constant velocity of 50 km/h
The model for generating this signal was presented in the following publication:
*Dreier, C. and Vorländer, M.: Road traffic auralization: Modeling and synthesis of electric drives, In: Proc. Tecniacústica, Cuenca, Spain (2023)*
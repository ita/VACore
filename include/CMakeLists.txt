target_sources (
	${LIB_TARGET}
	PRIVATE VACore.h
			VACoreDefinitions.h
			VACoreFactory.h
			VANetworkStreamAudioSignalSource.h
			VAObjectPool.h
			VAPoolObject.h
			VAReferenceableObject.h
			VAUncopyable.h
)
